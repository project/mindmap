# [Mind-map](https://drupal.emircanerkul.com) [styled] Offline Documentation [Drupal Module]

Mind-map styled Enriched Drupal Documentation. All documantation forked with [drupal-to-md](https://github.com/emircanerkul/drupal-to-md) from [Drupal Docs](https://www.drupal.org/docs) on 13 January 2023.

App is based on VueJS. If you want to twig the app click [here](https://github.com/emircanerkul/drupal-mind-map) to visit main repo.

This module **binds** dist assets into `/mindmap` route for `admins (access administration pages)` via `_preprocess_html`

![Preview](preview.png)

## Development

* Follow [main repo](https://github.com/emircanerkul/drupal-mind-map) instructions
* Do your things
* Build for drupal
* Update the dist folder

## Special Note & Thanks

Special thanks goes [*base project](https://github.com/wanglin2/mind-map) [contributors](https://github.com/wanglin2/mind-map/graphs/contributors) 

This project is rough and has not been thoroughly tested, its features are not
yet fully developed, and there are some performance issues. It is only for
learning and reference purposes and should not be used in actual projects.

The built-in themes and icons in the project come from:

* [Baidu Mind Map](https://naotu.baidu.com/)
* [Zhixi Mind Map](https://www.zhixi.com/)

## License

[![GitHub](https://img.shields.io/github/license/emircanerkul/drupal-to-md?style=for-the-badge)](LICENSE)
